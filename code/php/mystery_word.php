<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <?php

        // connexion à la base de donnée
        $bdd = mysqli_connect('localhost', 'root', '', 'mystery_word');
        if (mysqli_connect_errno()) {
            echo "connexion echoué" . mysqli_connect_errno();
        } 

        // Vous devez faire un select pour afficher le mot mystere affiché en bdd ! =)
        $request = mysqli_query($bdd, 'SELECT word FROM Mystery_word');

        // mysqli_result::fetch_array -- mysqli_fetch_array — Récupère la ligne suivante d'un ensemble de résultats sous forme de 
        // tableau associatif, numérique ou les deux
        /**
         * Retourne une ligne de données de l'ensemble de résultats et la renvoie sous forme de tableau. Chaque appel 
         * ultérieur à cette fonction 
         * renverra la ligne suivante dans l'ensemble de résultats, ou null s'il n'y a plus de lignes.En plus d'enregistrer les
         * données sous forme d'un tableau à indices numériques, elle peut aussi les enregistrer dans un tableau associatif, 
         * en utilisant les noms des champs comme clés. 
         * Si deux ou plusieurs colonnes du résultat ont le même nom, la dernière colonne sera 
         * prioritaire et écrasera toutes les données précédentes. Pour accéder aux autres colonnes du même nom, vous devez 
         * utiliser l'index numérique, ou faire un alias pour chaque colonne.
         */
        while($result = mysqli_fetch_array($request)) {

    ?>

    <p> Je veux connaitre le mot mystere, donc affichez le ici : <?php echo $result[0]; }?></p>

    <br>

    <a href="./update_profile.php" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">
        Modification du profile</a>
    
</body>
</html>