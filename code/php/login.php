<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Website CSS style -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Website Font style -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" href="../css/login.css">
		<title>Login</title>
	</head>
<body>
    <?php
        $bdd = mysqli_connect('localhost', 'root', '', 'mystery_word');
        if (mysqli_connect_errno()) {
            echo "connexion echoué" . mysqli_connect_errno();
        }

        if (!empty($_POST["email"]) && !empty($_POST["password"])) {
			$email = $_POST["email"];
			$password = $_POST["password"];

            // on fait la requete sql pour séclectioner le mot de passe de l'utilisateur qui correspond à l'email
            $query_password = "SELECT password FROM User WHERE email = '". htmlentities(addslashes($email), ENT_QUOTES)."'";

            // on crée une variable $result et on lui attribut la connexion à la base de donnée (bdd)
            // on lui attribut aussi la requete
            $result = mysqli_query($bdd, $query_password);

            if ($result) {
                // mysqli_fetch_assoc = permet de lire une ligne mysql dans un tableau associatif
                $row = mysqli_fetch_assoc($result);

                $password_stored = $row["password"];

                if (password_verify($password, $password_stored)) {
                    $_SESSION["email"] = $email;
                    header("location: ./mystery_word.php");

                /**
                * session_start() crée une session ou restaure celle trouvée sur le serveur, via l'identifiant de session passé 
                * dans une requête GET, POST ou par un cookie.
                * Lorsque session_start() est appelée ou lorsqu'une session démarre toute seule, PHP va appeler les gestionnaires 
                * d'ouverture et de lecture. Ce sont des gestionnaires internes fournis par PHP (comme fichiers, SQLite 
                * ou Memcached) ou encore des gestionnaires personnalisés définis au moyen de session_set_save_handler(). 
                * La fonction de lecture va récupérer toute session existante (stockée sous forme sérialisée) et va désérialiser 
                * les données pour peupler $_SESSION.
                */
                    session_start();
                } else {
                    header("location: ./login.php?error=1");

                    // exit = permet de stoper l'avancer du code
                    exit;
                }

            } else {
                header("location: ./login.php?error=1");
                exit;
            }
        }
    ?>
    <section id="login">
        <h3 class="text-center text-white pt-5">Login form</h3>
        <article class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="#" method="post">
                            <h3 class="text-center text-info">Login</h3>
                            <div class="form-group">
                                <label for="email" class="text-info">Email:</label><br>
                                <input type="email" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                            </div>
                            <div id="register-link" class="text-right">
                                <a href="./register.php" class="text-info">Register here</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>