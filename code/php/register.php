<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">


		<!-- Website CSS style -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="../css/register.css">
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Register</title>
	</head>
	<body>
		<?php
			$bdd = mysqli_connect('localhost', 'root', '', 'mystery_word');
			if (mysqli_connect_errno()) {
				echo "connexion echoué" . mysqli_connect_errno();
			}

			if (!empty($_POST["first_name"]) && !empty($_POST["last_name"])&& !empty($_POST["email"])
			&& !empty($_POST["password"])) {
				$first_name = $_POST["first_name"];
				$last_name = $_POST["last_name"];
				$email = $_POST["email"];
				$password = $_POST["password"];

				/* PASSWORD_DEFAULT- Utilisez l'algorithme bcrypt (par défaut à partir de PHP 5.5.0). 
				Notez que cette constante est conçue pour changer au fil du temps à mesure que de nouveaux 
				algorithmes plus puissants sont ajoutés à PHP. Pour cette raison, la longueur du résultat résultant de 
				l’utilisation de cet identifiant peut changer au fil du temps. Par conséquent, il est recommandé de stocker le 
				résultat dans une colonne de base de données pouvant contenir plus de 60 caractères (255 caractères seraient 
				un bon choix).*/

				$passwordHash = password_hash($password, PASSWORD_DEFAULT);

				// htmlentities = fonction qui permet de transformer tous les caractères html en un équivalent caractère sépciaux 
				// en php. chaque _ ou é vont se transformer pour que php puisse comprendre et le modifier

				/*
				htmlentities() , tous les caractères qui ont des équivalents d'entités de caractères HTML sont traduits 
				dans ces entités. La fonction get_html_translation_table() peut être utilisée pour renvoyer la table de 
				traduction utilisée en fonction des flagsconstantes fournies.
				*/

				/*
					addslashes = Son but principal est de reconnaître dans une chaîne de caractères les guillemets(‘), les guillemets doubles(“) 
					ainsi que les backslash(\) car l'utilité de ces caractères spéciaux sont d’ouvrir et fermer une chaîne de caractères.  
					Du coup, la fonction reconnaît que si il est utile qu’un tel est une simple apostrophe d’un mot ou non. Celà évite les
					erreurs ou une altération indésirable du code.
				*/


				$request = "INSERT INTO User(first_name, last_name, email, password) VALUES 
				('". htmlentities(addslashes($first_name), ENT_QUOTES)."', 
				'". htmlentities(addslashes($last_name), ENT_QUOTES)."',
				'". htmlentities(addslashes($email), ENT_QUOTES)."',
				'". htmlentities(addslashes($passwordHash), ENT_QUOTES)."'
				)";

				// bdd = on a repris la connexion à la base 
				// request = on a repris l'insertion de l'utilisateur dans la base de donnée
				$result = mysqli_query($bdd, $request);

				// on vérifie le $result et si c'est bon on redirige vers la connexion
				if ($result) {
					header("location: ./login.php");
				} else {
					// si c'est pas bon, on affiche une erreur
					$error = "erreur merci de compléter";
				}
			}
		?>
		<section class="container">
			<article class="row main">
				<div class="main-login main-center">
				<h5>Sign up once and watch any of our free demos.</h5>
					<form class="" method="post" action="#">
						
						<div class="form-group">
							<label for="first_name" class="cols-sm-2 control-label">Your First Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="first_name" id="first_name"  placeholder="Enter your First Name"/>
								</div>
							</div>
						</div>

                        <div class="form-group">
							<label for="last_name" class="cols-sm-2 control-label">Your Last Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="last_name" id="last_name"  placeholder="Enter your Last Name"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
								</div>
							</div>
						</div>
                        <div class="form-group">
							<a href="./login.php" class="text-info">Login here</a><br>
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                        </div>
					</form>
					<?php
						echo $error;
					?>
				</div>
			</article>
		</section>

		 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	</body>
</html>